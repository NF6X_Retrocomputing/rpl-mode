;;; rpl-mode.el --- Major mode for editing HP48gx User RPL source code.

;;; Copyright (C) 2016 Mark J. Blair <nf6x@nf6x.net>

;; Author:   Mark J. Blair <nf6x@nf6x.net>
;; Version:  0.1
;; Keywords: HP48 HP48gx rpl

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;; Notes:
;; https://groups.google.com/d/msg/comp.sys.hp48/tJXI-NdnuHM/uGmz4j7M6VIJ


;;; Commentary:
;; 
;; rpl-mode is a major mode for editing HP48gx User RPL source code in
;; Emacs. It is intended for editing ASCII text files with all non-ASCII
;; characters replaced with their backslash escape sequences, as
;; understood by translation mode 3 in the HP48gx's Kermit utility.
;; Source files should begin with a header which indicates the
;; translation mode used, e.g.:
;;
;;     %%HP: T(3)A(D)F(.);
;;
;; rpl-mode presently only provides syntax highlighting.
;; This is an early draft, with minimal testing.


;;; Code:

(defvar rpl-mode-hook nil)

(defvar rpl-mode-map
  (let ((map (make-sparse-keymap)))
  map)
  "Keymap for rpl major mode.")

(defconst rpl-builtin-regexp
  (regexp-opt '("\\<<" "\\>>" "CASE" "DIR" "DO" "ELSE" "END" "FOR"
		"IF" "IFT" "IFTE" "NEXT" "REPEAT"
		"START" "STEP" "THEN" "UNTIL" "WHILE"
		) 'words)

  "Matches builtins.")

(defconst rpl-keyword-regexp
  (regexp-opt '("\\<-A" "A\\->" "ABS" "ACK" "ACKALL" "ACOS" "ACOSH"
		"ADD" "AF" "ALOG" "AMORT" "AMRT" "AND" "ANIMATE" "APPLY" "ARC"
		"ARCHIVE" "AREA" "ARG" "ARRY\\->" "\\->ARRY" "ASIN" "ASINH" "ASN"
		"ASR" "ATAN" "ATANH" "ATICK" "ATTACH" "AUDO" "BAR" "BARPLOT" "BAUD"
		"BEEP" "BEG" "BESTFIT" "BIN" "BINS" "BLANK" "BOX" "BOXZ" "BUFLEN"
		"BYTES" "B\\->PV" "B\\->R" "CALC" "CANCL" "CASE" "CEIL" "CENTR" "CF"
		"%CH" "CHOOSE" "CHR" "CIRCL" "CKSM" "CLEAR" "CLK" "CLKADJ" "CLLCD"
		"CLOSEIO" "CL\\GS" "CLUSR" "CLVAR" "CNCT" "CNRM" "CNTR" "\\->COL"
		"+COL" "COL+" "-COL" "COL-" "COL\\->" "COL\\GS" "COLCT" "COMB" "CON"
		"COND" "CONIC" "CONJ" "CONLIB" "CONST" "CONT" "CONVERT" "COPY"
		"CORR" "COS" "COSH" "COV" "CR" "CRDIR" "CROSS" "CST" "CSWP" "CYLIN"
		"C\\->PX" "C\\->R" "\\<-D" "D\\->" "DARCY" "\\GSDAT" "DATE" "DATE+"
		"\\->DATE" "DBUG" "DDAYS" "DEC" "DECR" "DEFINE" "\\->DEF" "DEG"
		"DEL" "\\<-DEL" "DEL\\->" "DELALARM" "DELAY" "DELKEYS" "DEPND"
		"DEPTH" "DET" "DETACH" "\\->DIAG" "DIAG\\->" "DIFFEQ" "DINV" "DISP"
		"DNEG" "DOERR" "DOLIST" "DOSUBS" "DOT" "DOT+" "DOT-" "DRAW" "DRAX"
		"DROP" "DROPN" "DRPN" "DROP2" "DTAG" "DUP" "DUPN" "DUP2" "D\\->R"
		"e" "ECHO" "EDIT" "EEX" "EGV" "EGVL" "ENDSUB" "ENG" "EQ" "EQ\\->"
		"EQNLIB" "ERASE" "ERRM" "ERRN" "ERR0" "EVAL" "EXIT" "EXP" "EXPAN"
		"EXPND" "EXPFIT" "EXPM" "EXPR" "EXTR" "EYEPT" "E^" "E()" "F0\\Gl"
		"FACT" "FANNING" "FC?" "FC?C" "FFT" "FINDALARM" "FINISH" "FIX"
		"FLOOR" "FM," "FP" "FREE1" "FREEZE" "FS?" "FS?C" "FUNCTION" "FV"
		"F(X)" "F'" "GET" "GETI" "GOR" "GO\\|v" "GO\\->" "GRAD" "GRAPH"
		"GRIDMAP" "\\->GROB" "GXOR" "*H" "HALT" "HEAD" "HEX" "HISTOGRAM"
		"HISTPLOT" "HMS+" "HMS-" "HMS\\->" "\\->HMS" "HOME" "HZIN" "HZOUTN"
		"i" "IDN" "IM" "INCR" "INDEP" "INFO" "INFO?" "INFORM" "INIT+"
		"INPUT" "INS" "INV" "IP" "ISECT" "ISOL" "KEEP" "KERRM" "KEY" "KGET"
		"KILL" "LABEL" "LAST" "LASTARG" "LCD\\->" "\\->LCD" "LEVEL"
		"LIBEVAL" "LIBS" "LINE" "\\GSLINE" "LINFIT" "LININ" "LIST\\->"
		"\\->LIST" "\\GSLIST" "\\GPLIST" "\\GDLIST" "LN" "LNP1" "LOG"
		"LOGFIT" "LQ" "LR" "LSQ" "LU" "L*" "L()" "\\<-M" "M\\->" "MANT"
		"MARK" "MATCH" "\\|^MATCH" "\\|vMATCH" "MAX" "MAXR" "MAX\\GS"
		"MCALC" "MEAN" "MEM" "MENU" "MERGE1" "MIN" "MINEHUNT" "MINIT" "MINR"
		"MIN\\GS" "MITM" "ML" "MOD" "MOVE" "MSGBOX" "MROOT" "MSOLVR" "MUSER"
		"NDIST" "N\\GS" "NEG" "NEW" "NEWOB" "NOT" "NOVAL" "NSUB" "NUM"
		"NUMX" "NUMY" "\\->NUM" "NXEQ" "OBJ\\->" "OCT" "OFF" "OLDPRT"
		"OPENIO" "OR" "ORDER" "OVER" "PPAR" "\\GSPAR" "PARAMETRIC" "PARITY"
		"PARSURFACE" "PATH" "PCOEF" "PCONTOUR" "PCOV" "PDIM" "PERM" "PEVAL"
		"PGDIR" "PICK" "PICT" "PICT\\->" "PICTURE" "PINIT" "PIXOFF" "PIXON"
		"PIX?" "PKT" "PMAX" "PMIN" "PMT" "POLAR" "POS" "PRED" "PREDV"
		"PREDX" "PREDY" "PRINT" "PRLCD" "PROMPT" "PROOT" "PRST" "PRSTC"
		"PRTPAR" "PRVAR" "PR1" "PSDEV" "PURGE" "PUT" "PUTI" "PV" "PVAR"
		"PVARS" "PVIEW" "PWRFIT" "PYR" "PX\\->C" "\\->Q" "QR" "QUAD" "QUOTE"
		"\\->Q\\Gp" "RAD" "RAND" "RANK" "RANM" "RATIO" "RCEQ" "RCI" "RCIJ"
		"RCL" "RCALARM" "RCLF" "RCLKEYS" "RCLMENU" "RCL\\GS" "RCWS" "RDM"
		"RDZ" "RE" "RECN" "RECT" "RECV" "REPL" "RES" "RESET" "RESTORE"
		"REVLIST" "RKF" "RKFERR" "RKFSTEP" "RL" "RLB" "RND" "RNRM" "ROLL"
		"ROLLD" "ROOT" "ROT" "ROW+" "+ROW" "ROW-" "-ROW" "ROW\\->" "RR"
		"RRB" "RREF" "RRK" "RRKSTEP" "RSBERR" "RSD" "RWSP" "R\\->B" "R\\->C"
		"R\\->D" "SAME" "SBRK" "SCALE" "SCATRPLOT" "SCATTER" "SCHUR" "SCI"
		"SCL\\GS" "SCONJ" "SDEV" "SEND" "SEQ" "SERVER" "SF" "SHADE" "SHOW"
		"SIDENS" "SIGN" "SIMU" "SIN" "SINH" "SINV" "SIZE" "\\<-SKIP"
		"SKIP\\->" "SL" "SLB" "SLOPE" "SLOPEFIELD" "SNEG" "SNRM" "SOLVE"
		"SOLVEQN" "SORT" "SPHERE" "SQ" "SR" "SRAD" "SRB" "SRECV" "SST"
		"SST\\|v" "STD" "STEQ" "STIME" "\\|^STK" "\\->STK" "STO" "STOALARM"
		"STOF" "STOKEYS" "STO+" "STO-" "STO*" "STO/" "STO\GS" "STR\\->"
		"\\->STR" "STREAM" "STS" "STWS" "SUB" "SVD" "SVL" "SWAP" "SYM"
		"SYSEVAL" "\\<-T" "T\\->" "%T" "\\->TAG" "TAN" "TAIL" "TANH" "TANL"
		"TAYLR" "TDELTA" "TEACH" "TEXT" "TICKS" "\\->TIME" "TINC" "TLINE"
		"TMENU" "TOT" "TRACE" "TRANSIO" "TRG*" "\\->TRG" "TRN" "TRNC"
		"TRUTH" "TSTR" "TVARS" "TVM" "TVMBEG" "TVMEND" "TVMROOT" "TYPE"
		"UBASE" "UFACT" "\\->UNIT" "UPDIR" "UTPC" "UTPF" "UTPN" "UTPT"
		"UVAL" "VAR" "VARS" "VEC" "VIEW" "VPAR" "VTYPE" "VZIN" "VZOUT"
		"\->V2" "\\->V3" "V\\->" "*W" "WAIT" "WID\\->" "\\<-WID" "WIREFRAME"
		"WSLOG" "\\GSX" "\\GSX^2" "XCOL" "XMIT" "XOR" "XPON" "XRECV" "XRNG"
		"XROOT" "XSEND" "XVOL" "XXRNG" "\\GSX*Y" "X,Y\\->" "\\GSY" "\\GSY^2"
		"YCOL" "TRNG" "YSLICE" "YVOL" "YYRNG" "ZAUTO" "ZDECI" "ZDFLT"
		"ZFACT" "ZFACTOR" "ZIN" "ZINTG" "ZLAST" "ZOOM" "ZOUT" "ZSQR" "ZTRIG"
		"ZVOL" "+" "-" "*" "/" "^" "<" "\\<=" ">" "\\>=" "=" "==" "\\=/" "!"
		"\\.S" "\\Gd" "%" "\\Gp" "\\GS" "\\GS+" "\\GS-" "\\v/" "|" "\\->"
		) 'words)
  "Matches keywords.")

(defconst rpl-string-regexp
  "\"[^\"\n]*\""
  "Matches double-quoted strings.")

(defconst rpl-literal-regexp
  "'[^'\n]*'"
  "Matches single-quoted literals.")


(defvar rpl-font-lock-keywords
  (list
   `("^%%HP:.*;" . font-lock-preprocessor-face)
   `("@.*" . font-lock-comment-face)
   `(,rpl-string-regexp . font-lock-string-face)
   `(,rpl-literal-regexp . font-lock-string-face)
   `(,rpl-builtin-regexp . font-lock-builtin-face)
   `(,rpl-keyword-regexp . font-lock-keyword-face)
   )
  "Keyword highlighting for rpl mode.")

(defvar rpl-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?!  "w" st)
    (modify-syntax-entry ?%  "w" st)
    (modify-syntax-entry ?'  "w" st)
    (modify-syntax-entry ?*  "w" st)
    (modify-syntax-entry ?+  "w" st)
    (modify-syntax-entry ?,  "w" st)
    (modify-syntax-entry ?-  "w" st)
    (modify-syntax-entry ?/  "w" st)
    (modify-syntax-entry ?<  "w" st)
    (modify-syntax-entry ?=  "w" st)
    (modify-syntax-entry ?>  "w" st)
    (modify-syntax-entry ??  "w" st)
    (modify-syntax-entry ?\( "w" st)
    (modify-syntax-entry ?\) "w" st)
    (modify-syntax-entry ?\\ "w" st)
    (modify-syntax-entry ?^  "w" st)
    (modify-syntax-entry ?|  "w" st)
st)
  "Syntax table for `rpl-mode'.")

;;;###autoload
(defun rpl-mode ()
  "Major mode for editing HP48gx User RPL source code."
  (interactive)
  (kill-all-local-variables)
  (use-local-map rpl-mode-map)
  (set (make-local-variable 'font-lock-defaults)
       '(rpl-font-lock-keywords       ; keywords
         t                            ; keyword-only
         nil                          ; case-fold
         ;; syntax-alist
         ;; syntax-begin
         ;; ...
         ))
  (set-syntax-table rpl-mode-syntax-table)
  (setq major-mode 'rpl-mode)
  (setq mode-name "rpl")
  (run-hooks 'rpl-mode-hook))

(provide 'rpl-mode)

;;; rpl-mode.el ends here
