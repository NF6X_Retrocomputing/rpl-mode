# rpl-mode: Major mode for editing HP48gx User RPL source code.

## Description

rpl-mode is a major mode for editing HP48gx User RPL source code in
Emacs. It is intended for editing ASCII text files with all non-ASCII
characters replaced with their backslash escape sequences, as
understood by translation mode 3 in the HP48gx's Kermit utility.
Source files should begin with a header which indicates the
translation mode used, e.g.:

    %%HP: T(3)A(D)F(.);

## Limitations

* This is an early draft, with minimal testing.

* rpl-mode presently only provides syntax highlighting. It does not
  provide automatic indentation.

## Copyright and License

Copyright (C) 2016 Mark J. Blair <nf6x@nf6x.net>

rpl-mode is released under GPLv3. See [COPYING.md](COPYING.md) for license terms.
